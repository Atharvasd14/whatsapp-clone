import React from 'react'
import './Login.css';
import { Button } from '@material-ui/core';
import { auth } from './firebase';
import { provider } from './firebase';
import { useStateValue } from './StateProvider';
import { actionTypes } from './reducer';
function Login() {
    const [{},dispatch] = useStateValue();
    const signIn = () => {
  

        auth.signInWithPopup(
            provider
        ).then((result) =>
                {
                    dispatch({
                        type: actionTypes.SET_USER,
                        user: result.user,
                    });
                })
                .catch((error) => alert(error.message));
    };
    return (
        <div className = "login">
            <div className = "loginContainer">
                <div className = "loginText">
                    <h1>Sign in to WhatsApp</h1>
                </div>
                <Button onClick= {signIn} class = "loginButton">
                    Sign in with Google
                </Button>
            </div>
            
        </div>
    )
}

export default Login
