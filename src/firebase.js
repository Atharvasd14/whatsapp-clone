import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBmFhxMiq3v6aMBn8uKppXsfAnOkhClO74",
    authDomain: "whatsapp-b7dff.firebaseapp.com",
    databaseURL: "https://whatsapp-b7dff.firebaseio.com",
    projectId: "whatsapp-b7dff",
    storageBucket: "whatsapp-b7dff.appspot.com",
    messagingSenderId: "294496377503",
    appId: "1:294496377503:web:b9c8cc71fad0240ca5e0aa",
    measurementId: "G-RQ2KNWJCMQ"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebaseApp.firestore();
  const auth= firebase.auth();
  const provider = new firebase.auth.GoogleAuthProvider();
// const p = new firebase.auth.
  export {auth,provider}

  export default db;